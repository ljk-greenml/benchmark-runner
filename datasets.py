import os
import sys
import random
import json

DATASETS_FOLDER="./datasets/"

def get_all_configs():
    # where we will store all the datasets configs
    datasets_configs = []
    # for each folder in the dataset folder
    for subfolder in [x[0] for x in os.walk(DATASETS_FOLDER) if x[0].replace("/","").replace(".","")!=DATASETS_FOLDER.replace("/","").replace(".","")]:
        # for each json file
        for json_file in [jsf for jsf in os.listdir(subfolder) if jsf.endswith(".json")]:
            # concatenate the full path
            fullpath = subfolder+"/"+json_file
            # load the config and store it
            try:
                with open(fullpath) as f:
                    current_config = json.load(f)
                datasets_configs.append(current_config)
            # simply print a warning on failure
            except Exception as e:
                print("Unable to load configuration file", fullpath ,": ", e)
    # return configs
    random.shuffle(datasets_configs)
    return datasets_configs