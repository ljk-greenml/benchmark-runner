import requests
import time
import sys
import traceback
import random

BASE_MONITOR_ENDPOINT="http://84.7.38.211:30007/"

# les répliques de Frank la Terreur
repliques = [
    "Y'a rien qui marche dans votre projet",
    "Il me faudrait des anxiolytiques pour administrer votre bordel",
    "Bande de p'ti branleurs",
    "Ma tante Renée a eu 100ans et elle est plus solide que votre Pipeline",
    "C'est quel heure ? Ah ouais je m'en fou, je veux que ça marche moi",
    "Vos êtes tous virés vous entendez ? Virés ! regardez",
    "Le téléthon a apellé ils ont perdus 4 athlètes",
    "Vous avez un nouveau message. Message recu hier à RÉPAREZ MOI ÇA heure 45",
    "Le pentagone veut vous envoyer en Iran pour gérer le programme nucléaire",
    "Frank la terreur se moque de savoir si quelqu'un est fort ou intelligent. Tout ce qui compte, c'est que ça marche",
    "Certains disent que Lorys est Akatosh. Certains disent que Frank est un menteur. Ne croyez aucune de ces deux affirmations",
    "Mais dites donc, vous avez du travail",
    "Certains disent que Thomas est Akatosh. Certains disent que Rachdi c'était trompé pour la méthode du coude. Ne croyez aucune de ces deux affirmations",
    "À, À, À la queue leuleu ! Les bugs s'éclatent, à la queue leuleu",
    "Les derniers développeurs qui ont laissés des bugs comme ça, ils sont portés disparus",
    "Même la reine des neiges code mieux que vous",
    "Il a fait planter le logiciel :sob: Tout nu dans l'escalier ! Sinon pour le bug",
    "Celui qui a crée ce bug a un gage, il doit payer une bière aux autres",
    "Il y a un homme très sage un jour qui a dit: putain de merde nique sa mère ces bugs à la con j'arrête le dev",
    "Mon premier fait chier, mon deuxième est un développeur, mon troisième c'est vous trois",
    "Certains que Renan est Akatosh. Certains disent que la Renan Case c'est le futur. Ne croyez aucune de ces deux affirmations",
    "Le père de M'aiq s'appelait également M'aiq. Tout comme le père du père de M'aiq. Du moins, c'est ce que disait son père"
]

use_frank_the_terrible = True
def disable_frank():
    global use_frank_the_terrible
    use_frank_the_terrible = False 

def webhook_call(strmsg, trace):
    global use_frank_the_terrible

    if use_frank_the_terrible:
        url = "https://discord.com/api/webhooks/1072209521193271301/LMFTl8GX5P2CiINOeMzLknZFP97QeQ3MSDpJQNHsqadYDxflkYrvq4gtXbVZrEf_gx5K"
        data = {
            "content" : random.choice(repliques)+": \n"+strmsg+"```\n"+trace+"```",
            "username" : "Frank La Terreur"
        }

        result = requests.post(url, json = data)
        try:
            result.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print(err)
        else:
            print("Payload delivered successfully, code {}.".format(result.status_code))

    else:
        print(strmsg)
        print(trace)

required_metrics = [
    "accuracy", "precision", "recall", "f1", "specificity", "AUC", "log_loss",
    "jaccard_score", "precision_macro", "precision_micro", "recall_macro",
    "recall_micro", "f1_macro", "f1_micro", "adj_rand_index", "rand_index",
    "h_score", "fowlkes_mallows_index", "r2_score", "exp_var", "mse"
]

# make a call to the monitoring api to upload a chunk of the results (one algo over all)
def send_result_chunk(algo, config, measurement_value, metrics, key, instance, time_taken, size_train, size_test):
    url = BASE_MONITOR_ENDPOINT+"report/benchmark"
    body = {
        "secret_key": key,
        # general settings (partitioning keys)
        "instance": instance,
        "algorithm": algo,
        "dataset": config["name"],
        "task": config["task"],
        "measurement": config["measure"],
        # general settings (in rows)
        "timestamp": int(time.time()),
        "total_time_ms": time_taken,
        "consumption": measurement_value,
        "size_train": size_train,
        "size_test": size_test,
        "folds": config["folds"],
        # binary classif
        "accuracy": metrics["accuracy"],
        "precision": metrics["precision"],
        "recall": metrics["recall"],
        "f1": metrics["f1"],
        "specificity": metrics["specificity"],
        "auc": metrics["AUC"],
        "log_loss": metrics["log_loss"],
        "IoU": metrics["jaccard_score"],
        # multimodal classif
        "precision_macro": metrics["precision_macro"],
        "precision_micro": metrics["precision_micro"],
        "recall_macro": metrics["recall_macro"],
        "recall_micro": metrics["recall_micro"],
        "f1_macro": metrics["f1_macro"],
        "f1_micro": metrics["f1_micro"],
        # clustering
        "adjusted_randindex": metrics["adj_rand_index"],
        "rand_index": metrics["rand_index"],
        "h_score": metrics["h_score"],
        "fowlkes_mallows_index": metrics["fowlkes_mallows_index"],
        # regression
        "r2": metrics["r2_score"],
        "explained_variance_score": metrics["exp_var"],
        "MSE": metrics["mse"]
    }
    try:
        resp = requests.post(url, json = body)
        print("response received from server at result upload:", resp.text)
    except Exception as e:
        print("Unable to reach monitoring api to upload result:", e)
        sys.exit(1)

def add_missing_metrics(partial_metrics):
    all_metrics = partial_metrics
    for metric in required_metrics:
        if metric not in all_metrics:
            all_metrics[metric] = 0.0
    return all_metrics

def upload_benchmark_results(config, results, key, instance, time_taken):
    try:
        # list entries in the measurements map to get list of algos
        algos = results["measurement"].keys()
        # iterate over algos
        for algo in algos:

            # add missing metrics
            all_metrics = add_missing_metrics(results["metrics"][algo])

            # call result uplaod method
            send_result_chunk(
                algo,
                config,
                results["measurement"][algo],
                all_metrics,
                key,
                instance,
                time_taken,
                results["nb_lines_train"],
                results["nb_lines_test"]
            )

    # threatens the developers if their package had updated API
    except KeyError as keyer:
        print("The results expected by the Benchmark Runner are different from the one returned by GreenML Runners...")
        print("How dare you ? Here is the result:")
        print(results)
        traceback.print_exc()
        sys.exit(1)


