#!/bin/bash

if [ "${CI_TOKEN}" == "" ]
then
  echo "Ce script doit être lancé dans le ci avec la variable CI_TOKEN"
  exit 1
fi

# si le folder datasets existe...
if [ -d "datasets" ]
then
  # on le met à jour
  echo "mise à jour du dépot datasets..."
  cd datasets
  git pull origin master
  cd ..
# si il n'existe pas, on le pull
else
  echo "Clonage du dépot datasets..."
  git clone https://faidideq:${CI_TOKEN}@gricad-gitlab.univ-grenoble-alpes.fr/ljk-greenml/datasets
fi

# si le folder greenml existe...
if [ -d "greenml" ]
then
  # on le met à jour
  echo "mise à jour du dépot greenml..."
  cd greenml
  git pull origin master
  cd ..
# si il n'existe pas, on le pull
else
  echo "Clonage du dépot greenml..."
  git clone https://faidideq:${CI_TOKEN}@gricad-gitlab.univ-grenoble-alpes.fr/ljk-greenml/py_green_ml greenml
fi