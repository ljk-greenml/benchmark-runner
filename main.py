import argparse
import sys
import warnings
import time
import traceback
import copy
import random
from datasets import get_all_configs
from monitoring import upload_benchmark_results, webhook_call, disable_frank


# cli arg parsing
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--iterations", dest="iter", default=1, type=int, help="Number of runs over all datasets, 0 is infinite")
parser.add_argument("-k", "--key", dest="key", required=True, help="Secret key to upload monitoring results")
parser.add_argument("-n", "--name", dest="name", required=True, help="Name your instance !")
parser.add_argument('-S', '--silent', dest='silent', action='store_true', help='Shut the sklearn warnings off')
parser.add_argument('-H', '--hide', dest="hide", action="store_true", help="If set to true, will ignore the monitoring logs to send to server")
parser.add_argument('-N', '--no-frank', dest="no_frank", action="store_true", help="Disable frank messages")
parser.add_argument('-r', '--random-model', dest="random_model", action="store_true", help="Run a random model for each task instead of the entire model list")
args = parser.parse_args()

if args.no_frank:
    disable_frank()

# opening config files
all_configs = get_all_configs()
print("Found ", len(all_configs), " datasets/tasks configs")
if len(all_configs)==0:
    print("no valid config found, aborting")
    sys.exit(0)

# warning suppressing if necessary
def warn(*args, **kwargs):
    pass
if args.silent:
    warnings.warn = warn

# import here since it seem to be required to suppress warnings
# https://stackoverflow.com/questions/32612180/eliminating-warnings-from-scikit-learn
from greenml.runner.runner import Runner

# main loop to repeat
def run_all_benchmarks():
    # for each task/dataset combo
    for config_file in all_configs:
        try:
            print("Preparing to run task", config_file["task"], "on dataset", config_file["name"],"...")
            # create a copy of the config to eventually change its models
            config_modified = copy.deepcopy(config_file)
            if args.random_model:
                config_modified["mod_tokens"] = [ random.choice(config_modified["mod_tokens"]) ]
            print("about to run following models tokens: ", config_modified["mod_tokens"])
            # create runner
            current_runner = Runner(config_modified)
            # get benchmark results
            start_time = time.time()
            results = current_runner.run()
            time_taken = int(1000*(time.time() - start_time))
            print("Time taken for the task: ", time_taken, "milliseconds")
            # upload results (if not in hidden mode)
            if not args.hide:
                upload_benchmark_results(config_modified, results, args.key, args.name, time_taken)
        except Exception as e:
            print("Fix your stuff :", e)
            trace = traceback.format_exc()
            webhook_call(str(e), trace)

# run the main loop
if args.iter == 0:
    while True:
        print("New benchmark running...")
        run_all_benchmarks()
else:
    for i in range(args.iter):
        print("Running benchmark "+str(i+1)+" over "+ str(args.iter) + "...")
        run_all_benchmarks()