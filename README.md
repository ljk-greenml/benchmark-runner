# GreenML Benchmark Runner
Ce dépôt utilise les travaux du groupe GreenML pour lancer automatiquement
des benchmark de consommation et de performance sur des algorithmes et jeu
de données variés.

## Installation 


- Initialiser et dl les dépendances:
```sh
rm -rf datasets
rm -rf greenml
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:ljk-greenml/datasets.git
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:ljk-greenml/py_green_ml.git greenml
```

## Pour lancer un benchmark pour toutes le configurations disponibles

```sh
python main.py --key MySuperSecretKey! --name test_de_jean_claude --silent --iterations 1
# ou
python main.py -k MySuperSecretKey! -n test_de_jean_claude -S -i 1
```

## Plus d'infos

```sh
python main.py -h
```